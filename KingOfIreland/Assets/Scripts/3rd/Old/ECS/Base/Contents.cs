﻿using FP;
using System;
using System.Collections.Generic;


namespace MT.ECS
{
	public static class Contents
	{
		private static Dictionary<Type, ITable> tables = new();

		public static Table<T> GetTable<T>() where T : Component
		{
			Type type = typeof(T);
			if (!tables.ContainsKey(type))
				tables.Add(type, new Table<T>());
			if (tables[type] is Table<T> table)
				return table;
			else
				throw new Exception($"字典tables中，key为{typeof(T)}对应的对象不能为{tables[type].GetType()}！！！");
		}

		public static void Replace<T>(T t) where T : Component
		{
			GetTable<T>().Replace(t);
		}

		public static void Clear() => tables = new();

		public static bool HasComponent<T>(this Entity entity) where T : Component
			=> GetTable<T>()[entity.Id].isSome;

		public static bool TryToGetComponent<T>(this Entity entity, out Option<T> opt) where T : Component
		{
			opt = GetTable<T>()[entity.Id];
			return opt.isSome;
		}
		public static bool HasComponent(this Entity entity, Type t)
		{
			if (tables.TryGetValue(t, out ITable table))
			{
				return table.HasComponent(entity);
			}
			return false;
		}


		//public static List<long> GetEntitiesByTypes(HashSet<Type> types)
		//{
		//	return new();
		//}
	}
}