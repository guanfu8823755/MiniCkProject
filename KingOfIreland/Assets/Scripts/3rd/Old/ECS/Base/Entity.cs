﻿using FP;
using System;

namespace MT.ECS
{
	public struct Entity
	{
		public long Id { get; private set; }

		public static Entity New => new() { Id = T.GenerateId() };

		public static implicit operator Entity(long value) => new() { Id = value };
		public static implicit operator long(Entity e) => e.Id;
	}

	public static class EntityExt
	{

	}

}
