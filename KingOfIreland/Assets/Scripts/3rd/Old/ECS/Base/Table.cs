﻿using FP;
using System;
using System.Collections.Generic;
using System.Linq;

namespace MT.ECS
{
	using static F;

	interface ITable
	{
		public bool HasComponent(Entity entity);
	}
	public class Table<T> : ITable where T : Component
	{
		private readonly Dictionary<long, Stack<T>> cmtDic = new();

		public void Replace(T t)
			=> GetOrCreateStackBy(t.Id).Push(t);

		public void Remove(long id)
		{
			this[id].Match(
				() => None,
				c =>
				{
					Replace(c.With(c => c.IsRemoved, true));
					return None;
				});
		}
		public Option<T> this[long id]
		{
			get
			{
				if (cmtDic.ContainsKey(id))
				{
					T t = GetOrCreateStackBy(id).Peek();
					if (!GetOrCreateStackBy(id).Peek().IsRemoved)
					{
						return t;
					}
				}
				return null;
			}
		}

		/// <summary>
		/// 获取对应的堆栈，如果没有则创建一个
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		Stack<T> GetOrCreateStackBy(long id)
		{
			if (!cmtDic.ContainsKey(id))
				cmtDic.Add(id, new Stack<T>());
			return cmtDic[id];
		}

		public IEnumerable<T> Select()
			=> cmtDic.Values.Select(s => s.Peek()).Where(s => !s.IsRemoved);

		public IEnumerable<T> Select(Func<T, bool> func)
			=> cmtDic.Values.Select(s => s.Peek()).Where(s => !s.IsRemoved).Where(func);

		public bool HasComponent(Entity entity)
			=> this[entity.Id].isSome;
	}
}
