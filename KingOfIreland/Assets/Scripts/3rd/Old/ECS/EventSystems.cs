﻿using MT.ECS.EventBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MT.ECS
{
	public static class EventSystems
	{
		public static TriggerGroups ReplacedGroupToAdd;
		public static TriggerGroups ReplacedGroupToExecute;

		public static GengeralGroups gengeralGroups;

		public static Assembly assembly = typeof(EventSystems).Assembly;

		public static List<IInitSystem> InitSystems { get; private set; } = new();
		public static List<IUpdateSystem> UpdateSystems { get; private set; } = new();
		public static List<IOnReplaceSystem> OnReplaceSystems { get; private set; } = new();

		public static bool IsOn { get; private set; } = false;

		public static void Init()
		{
			ReplacedGroupToAdd = new();
			ReplacedGroupToExecute = new();
			gengeralGroups = new();

			var ts = (from type in assembly.GetTypes()
					  from inf in type.GetInterfaces()
					  where inf == typeof(IInitSystem) || inf == typeof(IUpdateSystem) || inf == typeof(IOnReplaceSystem)
					  group inf by type).ToList();


			assembly.GetTypes().Where(t => t.GetInterfaces().Contains(typeof(IInitSystem))).ToList()
				.ForEach(t =>
				{
					var obj = Activator.CreateInstance(t);
					if (obj is IInitSystem ini)
					{
						InitSystems.Add(ini);
					}
				});


		}

		public static void Start()
		{
			InitSystems.ForEach(i => i.Execute());
			IsOn = true;
		}
		public static void Update()
		{
			if (!IsOn)
				return;
			UpdateSystems.ForEach(
				us => us.Update(
					gengeralGroups
						.TryGetEntities(us.SystemTypes.ToCode())
					)
				);
			ReplacedGroupToExecute = ReplacedGroupToAdd;
			ReplacedGroupToAdd = new();

			OnReplaceSystems.ForEach(
				ors => ors.OnReplace(
					ReplacedGroupToExecute
						.GetEntitiesByCode(ors.SystemTypes.ToCode())
					)
				);

		}
	}
}