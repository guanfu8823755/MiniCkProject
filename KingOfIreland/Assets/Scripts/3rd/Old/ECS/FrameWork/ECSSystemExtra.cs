﻿using FP;
using FP.ImmutableExtra;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MT.ECS.EventBase;

namespace MT.ECS
{
	public static class ECSSystemExtra
	{

		public static Option<T> GetComponent<T>(this Entity entity) where T : Component
			=> Contents.GetTable<T>()[entity.Id];



		public static void Replace<T, P>(this Entity entity, Expression<Func<T, P>> func, object value) where T : Component
		{
			T t = ImmutableExtra.Create<T>()
				  .With(c => c.Id, entity.Id)
				  .With(c => c.IsRemoved, false)
				  .With(func, value)
				  .ToExecute();
			entity.Replace(t);
		}

		public static void Replace<T>(this Entity entity, T t) where T : Component
		{
			if (t.Id != entity)
				throw new Exception("要赋值的entity和component 的id 不同");
			Contents.Replace(t);
			EventSystems.ReplacedGroupToAdd.AddComponentToGroup(t);
		}

		public static void Remove<T>(this Entity entity) where T : Component
		{
			if (entity.TryToGetComponent(out Option<T> o))
			{
				T @new = o.value.With(c => c.IsRemoved, true);
				entity.Replace(@new);
			}
		}

		public static string ToCode(this HashSet<Type> types)
		=> string.Join("|", types.ToListWithSort());

		public static string JoinStrWith(this IEnumerable<string> strs, string splitChar)
			=> string.Join(splitChar, strs.ToListWithSort());



		public static IInitSystem CreateInitSystem(this Type type)
		{
			var obj = Activator.CreateInstance(type);
			if (obj is IInitSystem init)
			{
				return init;
			}
			return null;
		}

		public static IUpdateSystem CreateUpdateSystem(this Type type)
		{
			var obj = Activator.CreateInstance(type);
			if (obj is IUpdateSystem update)
			{
				EventSystems.gengeralGroups.AddGroup(new GeneralGroup(update.SystemTypes.ToArray()));
				return update;
			}
			return null;
		}
		public static IOnReplaceSystem CreateOnReplaceSystem(this Type type)
		{
			var obj = Activator.CreateInstance(type);
			if (obj is IOnReplaceSystem onReplace)
			{
				//EventSystems.ReplacedGroupToAdd.AddGroup(new GeneralGroup(onReplace.SystemTypes.ToArray()));
				return onReplace;
			}
			return null;
		}

	}
}
