﻿namespace MT.ECS.EventBase
{
	public interface IInitSystem
	{
		public abstract int SortNum { get; }
		public void Execute();
	}
}
