﻿using System;
using System.Collections.Generic;

namespace MT.ECS.EventBase
{
	public interface IOnReplaceSystem
	{
		public HashSet<Type> SystemTypes { get; }
		public void OnReplace(List<Entity> entities);
	}
}
