﻿using System;
using System.Collections.Generic;

namespace MT.ECS.EventBase
{
	/// <summary>
	/// 每帧都会调用的事件
	/// </summary>
	public interface IUpdateSystem
	{
		public HashSet<Type> SystemTypes { get; }
		public void Update(List<Entity> entities);
	}
}
