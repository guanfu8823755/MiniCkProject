﻿using System;
using System.Collections.Generic;

namespace MT.ECS
{
	public class GeneralGroup : GroupBase
	{
		private HashSet<long> Entities { get; set; } = new();
		public HashSet<Type> Types { get; private set; }
		public override string Code => string.Join("|", Types.ToListWithSort());
		public GeneralGroup(params Type[] types)
		{
			Types = new HashSet<Type>(types);
		}

		public override HashSet<long> Select()
			=> Entities;

		public override void AddToGroup(Entity entity)
			=> Entities.Add(entity.Id);
		public override void AddToGroup(long id)
			=> Entities.Add(id);

		public override bool IsSatisfied(Entity entity)
		{
			foreach (Type t in Types)
			{
				if (!entity.HasComponent(t))
					return false;
			}
			return true;
		}

		public override bool Remove(Entity entity) => Entities.Remove(entity.Id);
		public override bool Remove(long entityId) => Entities.Remove(entityId);
	}
}
