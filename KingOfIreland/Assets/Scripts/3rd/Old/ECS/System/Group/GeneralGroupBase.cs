﻿using System;
using System.Collections.Generic;

namespace MT.ECS
{
	public abstract class GroupBase
	{
		public abstract string Code { get; }

		public abstract void AddToGroup(long id);
		public abstract bool IsSatisfied(Entity entity);
		public abstract bool Remove(Entity entity);
		public abstract bool Remove(long entityId);
		public abstract HashSet<long> Select();
		public abstract void AddToGroup(Entity entity);
	}
}