﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MT.ECS
{
	public class GengeralGroups : IGeneralGroupBase
	{
		private Dictionary<Type, HashSet<string>> Type2Group { get; set; } = new();
		private Dictionary<string, GeneralGroup> GroupTbl { get; set; } = new();

		public void AddGroup(GeneralGroup group)
		{
			string code = group.Code;
			if (!GroupTbl.ContainsKey(code))
			{
				GroupTbl.Add(code, group);
				group.Types.ToList().ForEach(t =>
				{
					if (!Type2Group.ContainsKey(t))
						Type2Group.Add(t, new HashSet<string>());
					Type2Group[t].Add(code);
				});
			}
		}

		public List<Entity> TryGetEntities(string code)
		{
			if (GroupTbl.TryGetValue(code, out GeneralGroup group))
			{
				return group.Select().Select(l => (Entity)l).ToList();
			}

			return new();
		}

		public void OnComponentAdd<T>(T t) where T : Component
		{
			Type type = typeof(T);

			if (Type2Group.TryGetValue(type, out HashSet<string> groups))
				groups.ToList().ForEach(s =>
				{
					if (GroupTbl[s].IsSatisfied(t.Id))
						GroupTbl[s].AddToGroup(t.Id);
				});
		}

		public void OnComponentRemove<T>(T t) where T : Component
		{
			Type type = typeof(T);
			if (Type2Group.TryGetValue(type, out HashSet<string> groups))
				groups.ToList().ForEach(s => GroupTbl[s].Remove(t.Id));
		}


	}

	public interface IGeneralGroupBase
	{
		public void AddGroup(GeneralGroup group);
		public void OnComponentAdd<T>(T t) where T : Component;
		public void OnComponentRemove<T>(T t) where T : Component;
		public List<Entity> TryGetEntities(string code);
	}
}
