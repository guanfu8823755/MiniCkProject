﻿using System;
using System.Collections.Generic;

namespace MT.ECS
{
	public class TriggerGroup
	{
		private HashSet<long> entities = new();
		public HashSet<Type> Types { get; private set; }
		public string Code => string.Join("|", Types.ToListWithSort());
		public TriggerGroup(params Type[] types)
		{
			Types = new HashSet<Type>(types);
		}

		public HashSet<long> Select()
		{
			var res = entities;
			entities = new HashSet<long>();
			return res;
		}

		private void AddToGroup(Entity entity)
			=> entities.Add(entity.Id);
		public void AddToGroup(long id)
			=> entities.Add(id);
	}
}
