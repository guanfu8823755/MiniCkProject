﻿using System;
using System.Collections.Generic;
using System.Linq;
using MT;
using FP;
using Unit = System.ValueTuple;
namespace MT.ECS
{
	public class TriggerGroups
	{
		private Dictionary<Type, HashSet<string>> Type2Group { get; set; } = new Dictionary<Type, HashSet<string>>();
		private Dictionary<string, TriggerGroup> GroupTbl { get; set; } = new Dictionary<string, TriggerGroup>();

		public void AddGroup(TriggerGroup group)
		{
			string code = group.Code;
			if (!GroupTbl.ContainsKey(code))
			{
				GroupTbl.Add(code, group);
				group.Types.ToList().ForEach((t) =>
				{
					if (!Type2Group.ContainsKey(t))
						Type2Group.Add(t, new HashSet<string>());
					Type2Group[t].Add(code);
				});
			}
		}

		public void AddComponentToGroup(Component component)
		{
			Type t = component.GetType();
			long id = component.Id;
			if (Type2Group.TryGetValue(t, out HashSet<string> hs))
			{
				hs.ToList().ForEach(s =>
				{
					GroupTbl[s].AddToGroup(id);
				});
			}
		}



		public List<Entity> GetEntitiesByCode(string code)
		{
			if (GroupTbl.TryGetValue(code, out TriggerGroup group))
			{
				return group.Select().Select(l => (Entity)l).ToList();
			}
			return new();
		}


	}
}
