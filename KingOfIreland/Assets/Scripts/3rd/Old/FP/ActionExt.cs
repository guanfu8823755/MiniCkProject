﻿using System;
using Unit = System.ValueTuple;

namespace FP
{
	using static F;
	public static class ActionExt
	{
		public static Action UnitAct = () => { };

		public static Func<Unit> None = UnitAct.ToFunc();
		public static Func<Unit> ToFunc(this Action action)
			=> () => { action(); return Unit(); };

		public static Func<T, Unit> ToFunc<T>(this Action<T> action)
		  => t => { action(t); return Unit(); };

		public static Func<T1, T2, Unit> ToFunc<T1, T2>(this Action<T1, T2> action)
			=> (T1 t1, T2 t2) => { action(t1, t2); return Unit(); };
		public static Func<T1, T2, T3, Unit> ToFunc<T1, T2, T3>(this Action<T1, T2, T3> action)
			=> (T1 t1, T2 t2, T3 t3) => { action(t1, t2, t3); return Unit(); };
		public static Func<T1, T2, T3, T4, Unit> ToFunc<T1, T2, T3, T4>(this Action<T1, T2, T3, T4> action)
			=> (T1 t1, T2 t2, T3 t3, T4 t4) => { action(t1, t2, t3, t4); return Unit(); };
		public static Func<T1, T2, T3, T4, T5, Unit> ToFunc<T1, T2, T3, T4, T5>(this Action<T1, T2, T3, T4, T5> action)
			=> (T1 t1, T2 t2, T3 t3, T4 t4, T5 t5) => { action(t1, t2, t3, t4, t5); return Unit(); };
		public static Func<T1, T2, T3, T4, T5, T6, Unit> ToFunc<T1, T2, T3, T4, T5, T6>(this Action<T1, T2, T3, T4, T5, T6> action)
			=> (T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6) => { action(t1, t2, t3, t4, t5, t6); return Unit(); };
		public static Func<T1, T2, T3, T4, T5, T6, T7, Unit> ToFunc<T1, T2, T3, T4, T5, T6, T7>(this Action<T1, T2, T3, T4, T5, T6, T7> action)
			=> (T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7) => { action(t1, t2, t3, t4, t5, t6, t7); return Unit(); };
		public static Func<T1, T2, T3, T4, T5, T6, T7, T8, Unit> ToFunc<T1, T2, T3, T4, T5, T6, T7, T8>(this Action<T1, T2, T3, T4, T5, T6, T7, T8> action)
			=> (T1 t1, T2 t2, T3 t3, T4 t4, T5 t5, T6 t6, T7 t7, T8 t8) => { action(t1, t2, t3, t4, t5, t6, t7, t8); return Unit(); };
	}
}
