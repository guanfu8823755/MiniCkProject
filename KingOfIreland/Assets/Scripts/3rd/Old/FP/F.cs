﻿using System;

namespace FP
{
	using Unit = System.ValueTuple;
	public static partial class F
	{
		/// <summary>
		/// 便利方法允许你将返回Unit的函数简写为 return Unit()
		/// </summary>
		/// <returns></returns>
		public static Unit Unit() => default;




		public static R Using<TDisp, R>(TDisp disposable, Func<TDisp, R> f) where TDisp : IDisposable
		{
			using (disposable)
				return f(disposable);
		}


		public static (T, T) Swap<T>(T t1, T t2) => (t2, t1);



	}


}
