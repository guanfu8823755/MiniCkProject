﻿using FP.ImmutableExtra;
using System;
using System.Linq.Expressions;

namespace FP
{
	public static class Immutable
	{

		public static T With<T>(this T source, string propertyName, object newValue)
		   where T : class
		{
			T @new = source.ShallowCopy();

			typeof(T).GetBackingField(propertyName)
			   .SetValue(@new, newValue);

			return @new;
		}

		public static T With<T, P>(this T source, Expression<Func<T, P>> exp, object newValue)
		   where T : class
		   => source.With(exp.MemberName(), newValue);
	}
}

namespace FP.ImmutableExtra
{
	public static class ImmutableExtra
	{
		static T As<T>(this T source, string propertyName, object newValue) where T : class
		{
			typeof(T).GetBackingField(propertyName)
			   .SetValue(source, newValue);
			return source;
		}
		static T As<T, P>(this T source, Expression<Func<T, P>> exp, object newValue) where T : class
		=> source.As(exp.MemberName(), newValue);

		public static Func<T> Create<T>() => () => Activator.CreateInstance<T>();

		public static Func<T> With<T, P>(this Func<T> source, Expression<Func<T, P>> exp, object newValue) where T : class
		=> () => source().As(exp, newValue);

		public static T ToExecute<T>(this Func<T> func) => func();
	}
}
