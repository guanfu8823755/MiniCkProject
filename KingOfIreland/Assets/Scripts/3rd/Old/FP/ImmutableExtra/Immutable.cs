﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace FP.ImmutableExtra
{
	public static class ImmutableBase
	{

		public static string MemberName<T, P>(this Expression<Func<T, P>> e)
		   => ((MemberExpression)e.Body).Member.Name;

		public static T ShallowCopy<T>(this T source)
		   => (T)source.GetType().GetTypeInfo().GetMethod("MemberwiseClone"
				 , BindingFlags.Instance | BindingFlags.NonPublic)
			  .Invoke(source, null);

		public static string BackingFieldName(string propertyName)
		   => string.Format("<{0}>k__BackingField", propertyName);

		public static FieldInfo GetBackingField(this Type t, string propertyName)
		   => t.GetTypeInfo().GetField(BackingFieldName(propertyName)
			  , BindingFlags.Instance | BindingFlags.NonPublic);
	}
}
