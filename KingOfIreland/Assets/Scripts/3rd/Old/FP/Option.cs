﻿using System;
using System.Collections.Generic;

namespace FP
{
	using static F;
	public static partial class F
	{
		public static Option.None None => Option.None.Default;
		public static Option<T> Some<T>(T value) => new Option.Some<T>(value);

	}

	public struct Option<T> : IEquatable<Option.None>, IEquatable<Option<T>>
	{
		public readonly bool isSome;
		public readonly T value;
		bool IsNone => !isSome;

		private Option(T value)
		{
			if (value == null)
				throw new ArgumentNullException();
			this.isSome = true;
			this.value = value;
		}
		/// <summary>
		/// 将None转化为Option
		/// </summary>
		/// <param name="_"></param>
		public static implicit operator Option<T>(Option.None _) => new();

		public static implicit operator Option<T>(Option.Some<T> some) => new(some.Value);

		public static implicit operator Option<T>(T value)
			=> value == null ? None : Some(value);

		public R Match<R>(Func<R> None, Func<T, R> Some)
			=> isSome ? Some(value) : None();

		public IEnumerable<T> AsEnumerable()
		{
			if (isSome) yield return value;
		}

		public bool Equals(Option<T> other)
			=> this.isSome == other.isSome && (this.IsNone || this.value.Equals(other.value));

		public bool Equals(Option.None _) => IsNone;
		public static bool operator ==(Option<T> @this, Option<T> other) => @this.Equals(other);
		public static bool operator !=(Option<T> @this, Option<T> other) => !(@this == other);

		public override string ToString() => isSome ? $"Some({value})" : "None";
	}

	namespace Option
	{
		/// <summary>
		/// None没有成员，因为它不包含任何数据
		/// </summary>
		public struct None
		{
			internal static readonly None Default = new();
		}
		public struct Some<T>
		{
			/// <summary>
			/// Some只包装了一个值
			/// </summary>
			internal T Value { get; }
			internal Some(T value)
			{
				if (value == null)
					throw new ArgumentNullException(nameof(value)
				  , "Cannot wrap a null value in a 'Some'; use 'None' instead");//some不接受null值
				Value = value;
			}
		}

	}


	public static class OptionExt
	{
		public static Option<R> Map<T, R>(this Option.None _, Func<T, R> f)
			=> None;

		public static Option<R> Map<T, R>(this Option.Some<T> some, Func<T, R> f)
		   => Some(f(some.Value));

		public static Option<R> Map<T, R>(this Option<T> optT, Func<T, R> f)
			=> optT.Match(
				() => None,
				(t) => Some(f(t)));
	}
}

