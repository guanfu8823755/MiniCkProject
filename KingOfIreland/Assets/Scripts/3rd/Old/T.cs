﻿using FP;
using System;
using System.Collections.Generic;
using System.Linq;
namespace MT
{
	using static F;
	public static class T
	{
		public static long GenerateId()
		{
			byte[] buffer = Guid.NewGuid().ToByteArray();
			return BitConverter.ToInt64(buffer, 0);
		}

		public static void WriteLine(this string msg) => Console.WriteLine(msg);

		public static List<T> ToListWithSort<T>(this IEnumerable<T> enus)
		{
			var list = enus.ToList();
			list.Sort();
			return list;
		}

		//public static Option<TV> GetValue<TK, TV>(this Dictionary<TK, TV> dic, TK key)
		//{
		//	if (dic.ContainsKey(key))
		//		return dic[key];
		//	return None;
		//}
	}
}
