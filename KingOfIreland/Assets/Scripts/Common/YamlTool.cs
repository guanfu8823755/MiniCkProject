﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.Serialization;


public static class Yaml
{
	static Deserializer deserializer = new Deserializer();
	static Serializer serializer = new Serializer();
	public static T Deserialize<T>(string file)
	{
		return deserializer.Deserialize<T>(file);
	}

	public static string Serialize<T>(T t)
	{
		return serializer.Serialize(t);
	}
}

