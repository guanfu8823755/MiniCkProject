//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------
using Bright.Serialization;
using System.Collections.Generic;
using SimpleJSON;



namespace cfg.Game
{ 

public sealed partial class TbAreaCounty
{
    private readonly Dictionary<int, Game.AreaCounty> _dataMap;
    private readonly List<Game.AreaCounty> _dataList;
    
    public TbAreaCounty(JSONNode _json)
    {
        _dataMap = new Dictionary<int, Game.AreaCounty>();
        _dataList = new List<Game.AreaCounty>();
        
        foreach(JSONNode _row in _json.Children)
        {
            var _v = Game.AreaCounty.DeserializeAreaCounty(_row);
            _dataList.Add(_v);
            _dataMap.Add(_v.Id, _v);
        }
        PostInit();
    }

    public Dictionary<int, Game.AreaCounty> DataMap => _dataMap;
    public List<Game.AreaCounty> DataList => _dataList;

    public Game.AreaCounty GetOrDefault(int key) => _dataMap.TryGetValue(key, out var v) ? v : null;
    public Game.AreaCounty Get(int key) => _dataMap[key];
    public Game.AreaCounty this[int key] => _dataMap[key];

    public void Resolve(Dictionary<string, object> _tables)
    {
        foreach(var v in _dataList)
        {
            v.Resolve(_tables);
        }
        PostResolve();
    }

    public void TranslateText(System.Func<string, string, string> translator)
    {
        foreach(var v in _dataList)
        {
            v.TranslateText(translator);
        }
    }
    
    
    partial void PostInit();
    partial void PostResolve();
}

}