﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
	public enum Sex
	{
		Male,
		Female,
	}
	public enum LifeState
	{
		Health,//健康
		OnDeathCheck,//死亡检测中
		ToDie,//已确定死亡日期
	}
	public class Character
	{
		public int Id { get; set; }
		public string Name { get; set; }   
		public Sex Sex { get; set; }
		public int Age { get; set; }
		public float Health { get; set; }
		public LifeState LifeState { get; set; }
		public Date BirthDay { get; set; }
	}
}
