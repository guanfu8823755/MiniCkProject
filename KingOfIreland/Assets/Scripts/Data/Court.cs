﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{

	public class Court
	{
		public int Id { get; set; }
		public int OwnerId { get; set; }
		public List<int> OwnerTitleIds { get; set; }
	}

	public class Title
	{
		public int Id { get; set; }
		public int CourtId { get; set; }
		public int OwnerId { get; set; }
		public int Level { get; set; }
		public bool IsHonorary { get; set; }
		public int AreaId { get; set; }

	}
}
