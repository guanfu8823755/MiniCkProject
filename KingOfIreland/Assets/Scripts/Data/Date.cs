﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data
{
	public class Date
	{


		public int TimeStamp { get; set; }
		public int Year { get; set; }
		public int Month { get; set; }
		public int Day { get; set; }
		public Date(int year, int month, int day)
		{
			Year = year;
			Month = month;
			Day = day;
			TimeStamp = DateUtil.GetTimeStamp(year, month, day);
		}
	}

	public static class DateUtil
	{
		public static int[] Days = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
		public static void Next(this Date date)
		{
			date.TimeStamp++;
			date.Day++;
			if (date.Day > Days[date.Month - 1])
			{
				date.Day -= Days[date.Month - 1];
				date.Month++;
				if (date.Month > 12)
				{
					date.Month -= 12;
					date.Year++;
				}
			}
		}
		public static int GetTimeStamp(int year, int month, int day)
		{
			int Stamp = 0;
			Stamp += year * 365;
			if (month > 1)
			{
				for (int i = 0; i < month - 1; i++)
				{
					Stamp += Days[i];
				}
			}
			Stamp += day;
			return Stamp;
		}
	}
}
