using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data;
using YamlDotNet.Serialization;
using System;
using cfg;
using SimpleJSON;

public class Game : MonoBehaviour
{
	public static Game Instance { get; private set; }
	public Tables Configs;
	/// <summary>
	/// 需要保存的数据
	/// </summary>
	private World world;

	#region 临时数据
	/// <summary>
	/// 玩家是否可以操作
	/// </summary>
	private bool IsPlayerTime;
	private bool IsPause;
	private int ForcedPauseNum;
	private int SpeedLevel;
	private List<float> waitTime = new();
	#endregion

	/// <summary>
	/// 初始化游戏
	/// </summary>
	private void InitGame()
	{

		InitConfig();
		InitStates();
		InitWorld();
	}

	private void InitWorld()
	{
		world = new World();
		world.Init();
	}

	private void InitConfig()
	{
		Configs = new Tables(LoadByteBuf);

		JSONNode LoadByteBuf(string file)
		{
			string path = "Defines/" + file;
			string fileText = Resources.Load<TextAsset>(path).text;
			JSONNode json = JSON.Parse(fileText);
			return json;
		}
	}


	private void InitStates()
	{
		IsPlayerTime = true;
		IsPause = true;
		ForcedPauseNum = 0;
		SpeedLevel = 0;
		waitTime.Add(Configs.TbConfig.SpeedLevel1);
		waitTime.Add(Configs.TbConfig.SpeedLevel2);
		waitTime.Add(Configs.TbConfig.SpeedLevel3);
		waitTime.Add(Configs.TbConfig.SpeedLevel4);
		waitTime.Add(Configs.TbConfig.SpeedLevel5);
	}

	public void ChangePause()
	{
		if (ForcedPauseNum > 0)
			return;
		if (IsPause)
		{
			IsPause = false;
			NextTurn();
		}
		else
		{
			IsPause = true;
		}
	}
	private void NextTurn()
	{
		IsPlayerTime = false;
		//各个AI的回合
		world.AiTurns();
		//回合结算
		world.RoundSettleMent();
		IsPlayerTime = true;
		if (ForcedPauseNum == 0 && (!IsPause))
		{
			Timer.SetTimeout(waitTime[SpeedLevel], NextTurn);
		}
	}
	private void Awake()
	{
		Instance = this;
	}
	private void Start()
	{
		InitGame();
	}
	private void Update()
	{
		if (Input.GetKeyUp(KeyCode.Space))
		{
			ChangePause();
		}
		if (Input.GetKeyUp(KeyCode.Keypad1))
		{
			SpeedLevel = 0;
		}
		if (Input.GetKeyUp(KeyCode.Keypad2))
		{
			SpeedLevel = 1;
		}
		if (Input.GetKeyUp(KeyCode.Keypad3))
		{
			SpeedLevel = 2;
		}
		if (Input.GetKeyUp(KeyCode.Keypad4))
		{
			SpeedLevel = 3;
		}
		if (Input.GetKeyUp(KeyCode.Keypad5))
		{
			SpeedLevel = 4;
		}
	}
}