using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer), typeof(MeshCollider))]
public abstract class BaseMesh : MonoBehaviour
{
	protected MeshFilter targetFilter;
	protected MeshCollider targetCollider;
	protected Mesh mesh;
	protected int[] tris;
	protected Vector2[] uvs;
	protected Vector3[] normals;

	// Use this for initialization
	void Awake()
	{
		targetFilter = GetComponent<MeshFilter>();
		targetCollider = GetComponent<MeshCollider>();
	}
	protected void Clear()
	{
		mesh = null;
		tris = null;
		uvs = null;
		normals = null;
	}
}
