using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SerializeField]
public enum EdgeType
{
	Default = 0,//默认(男爵领边界)
	UnionArea,//同一领主下伯爵领边界
	RullerEdge,//领主边界
	MyEdge,//我的领地
}
public class MapEdgeDrawer : MonoBehaviour
{
	public List<MapEdgeLineDrawer> Lines = new();
	public MapEdgeLineDrawer EdgeLine;

	public void Draw(List<Vector3> Points, EdgeType edgeType)
	{
		int length = Points.Count;
		for (int i = 0; i < length - 1; i++)
		{
			var line = Instantiate(EdgeLine);
			line.transform.position = Vector3.zero;
			line.Draw(Points[i], Points[i + 1], edgeType);

			line.transform.SetParent(this.transform);
			Lines.Add(line);
		}
	}

	public void UpdateMaterial(EdgeType edgeType)
	{
		Lines.ForEach(l => l.UpdateMaterial(edgeType));
	}
}
