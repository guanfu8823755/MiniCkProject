using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class MapEdgeLineDrawer : MonoBehaviour
{
	public List<Material> Materials = new();
	private float distance;
	LineRenderer line;
	[SerializeField]
	public Dictionary<EdgeType, float> EdgeWidth = new()
	{
		{EdgeType.Default,0.05f },
		{EdgeType.UnionArea,0.1f },
		{EdgeType.RullerEdge,0.15f },
		{EdgeType.MyEdge,0.2f },
	};
	private void Awake()
	{
		line = GetComponent<LineRenderer>();
	}

	public void Draw(Vector3 v1, Vector3 v2, EdgeType type)
	{
		line.material = Materials[(int)type];
		line.positionCount = 2;
		line.SetPositions(new Vector3[] { v1, v2 });
		var Wdith = EdgeWidth[type];
		line.startWidth = Wdith;
		line.endWidth = Wdith;
		distance = Vector3.Distance(v1, v2) / 2;
		line.textureScale = new Vector2(distance / Wdith, 1);
	}
	public void UpdateMaterial(EdgeType type)
	{
		var Wdith = EdgeWidth[type];
		line.startWidth = Wdith;
		line.endWidth = Wdith;
		line.textureScale = new Vector2(distance / Wdith, 1);
		line.material = Materials[(int)type];
	}
}
