using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

public class MapManager : MonoBehaviour
{
	public static MapManager Instance;
	public TextAsset MapConfig;
	public MapData Data;
	public Transform Cities;
	public Transform EdgeRoot;
	public Transform TitleRoot;

	public MapEdgeDrawer EdgeDrawer;
	public TextMeshPro text;
	public float TitleScale = 0.6f;

	private Dictionary<int, PolygonMesh> CityMeshes = new();
	private Dictionary<int, TextMeshPro> BaronyTitles = new();

	private Vector3 height = new Vector3(0, 0, -0.1f);

	private RectangleTool rtool = new();
	private void Awake()
	{
		Instance = this;
	}
	// Start is called before the first frame update
	void Start()
	{
		Data = Yaml.Deserialize<MapData>(MapConfig.text);
		CreateCities();
		SetCityColor();
		CreateEdgesByEdgeId(Data.Edges.Keys.ToHashSet());
	}
	#region ��������
	private void CreateCities()
	{
		foreach (var kp in Data.Cities)
		{
			List<Vector3> points = new();
			var city = kp.Value;
			var cityEdges = city.CityEdges.Select(v => Data.Edges[v]).ToList();
			var firstEdge = cityEdges[0];
			cityEdges.Remove(firstEdge);
			var startPoint = firstEdge.StartPoint();
			for (int i = 0; i < firstEdge.Points.Count - 1; i++)
			{
				points.Add(Data.Points[firstEdge.Points[i]].Position.ToVector3());
			}

			var endPoint = firstEdge.EndPoint();

			while (startPoint != endPoint)
			{
				firstEdge = cityEdges.First(ce => ce.StartPoint() == endPoint || ce.EndPoint() == endPoint);
				cityEdges.Remove(firstEdge);
				if (firstEdge == null)
				{
					Debug.LogError("���󣡣�����");
				}
				if (firstEdge.StartPoint() == endPoint)
				{
					endPoint = firstEdge.EndPoint();
					for (int i = 0; i < firstEdge.Points.Count - 1; i++)
					{
						points.Add(Data.Points[firstEdge.Points[i]].Position.ToVector3());
					}
				}
				else
				{
					endPoint = firstEdge.StartPoint();
					for (int i = firstEdge.Points.Count - 1; i > 0; i--)
					{
						points.Add(Data.Points[firstEdge.Points[i]].Position.ToVector3());
					}
				}
			}
			var City = CreateCity($"city_{kp.Key}", kp.Key, points);
			CityMeshes.Add(kp.Key, City);
		}
	}

	private PolygonMesh CreateCity(string Name, int id, List<Vector3> Points)
	{
		var pmesh = new GameObject(Name).AddComponent<PolygonMesh>();
		pmesh.transform.position = Vector3.zero;
		pmesh.transform.rotation = Quaternion.identity;
		pmesh.transform.localScale = Vector3.one;
		pmesh.DrawMeshOfList(id, Points);
		pmesh.transform.SetParent(Cities);
		return pmesh;
	}
	#endregion

	private void CreateEdgesByEdgeId(HashSet<string> ids)
	{
		ids.ToList().ForEach(e =>
		{
			var edge = Data.Edges[e];
			var edgeType = EdgeType.Default;
			if(edge.Cities.Count==2)
			{
				var BaronyTbl = Game.Instance.Configs.TbAreaBarony;
				int city_1 = edge.Cities[0];
				int city_2 = edge.Cities[1];
				if (BaronyTbl.Get(city_1).CountyId == BaronyTbl.Get(city_2).CountyId)
				{
					
				}else
				{
					edgeType = EdgeType.UnionArea;
				}
			}
			else
			{
				edgeType = EdgeType.UnionArea;
			}
			
			var edgeDrawer = Instantiate(EdgeDrawer);
			edgeDrawer.transform.position = Vector3.zero;
			var ps = edge.Points.Select(pid => Data.Points[pid].Position.ToVector3() + height).ToList();
			edgeDrawer.Draw(ps, edgeType);
			edgeDrawer.transform.SetParent(EdgeRoot);
		});
	}

	private void SetCityColor()
	{
		Game.Instance.Configs.TbAreaCounty.DataList.ForEach(county =>
		{
			Color c = Random.ColorHSV();
			while (c.r > 0.5f & c.g > 0.5f & c.b > 0.5f)
			{
				c = Random.ColorHSV();
			}
			county.BaronyList.ForEach(i => { CityMeshes[i].SetColor(c); });
			CreateTitles(county.BaronyList, county.NameKey);
		});
	}
	private void CreateTitles(List<int> cities, string name)
	{
		var _cities = cities.Select(i => i).ToList();
		int city = _cities[0];
		_cities.Remove(city);
		List<List<int>> cs = new();
		List<int> area = new();
		area.Add(city);
		cs.Add(area);
		while (_cities.Count > 0)
		{
			var new_city = _cities.FirstOrDefault(c =>
			{
				foreach(var ct in area)
				{
					if (Data.Cities[c].Roads.Contains(ct))
						return true;
				}
				return false;
			});
			if (new_city == 0)
			{
				area = new();
				cs.Add(area);
				city = _cities[0];
				_cities.Remove(city);
				area.Add(city);
			}
			else
			{
				city = new_city;
				_cities.Remove(city);
				area.Add(city);
			}
		}
		cs.ForEach(area =>
		{
			CreateOneTitle(area, name);
		});
		
	}

	private void CreateOneTitle(List<int> cities, string name)
	{
		var rect = GetRectangle(cities);
		float width = Vector3.Distance(rect.boundary[0], rect.boundary[1]);
		float height = Vector3.Distance(rect.boundary[1], rect.boundary[2]);
		var title = Instantiate(text);
		title.text = name;
		title.gameObject.name = "Title_" + name;
		int angle = 0;
		if (width > height)
		{
			title.rectTransform.sizeDelta = new Vector2(width, height);
		}
		else
		{
			title.rectTransform.sizeDelta = new Vector2(height, width);
			angle = 90;
		}
		title.rectTransform.position = (rect.boundary[0] + rect.boundary[1] + rect.boundary[2] + rect.boundary[3]) / 4f;
		title.rectTransform.localEulerAngles = new Vector3(0, 0, -rect.theta + angle);
		title.rectTransform.localScale = new Vector2(TitleScale, TitleScale);
		title.transform.SetParent(TitleRoot);
	}

	private RectangleTool.RectData GetRectangle(List<int> cityIds)
	{
		var cities = cityIds.Select(cid => Data.Cities[cid]).ToList();

		var v3s = cities.SelectMany(c => c.CityEdges).SelectMany(e => Data.Edges[e].Points).Select(p => Data.Points[p].Position.ToVector3()).ToList();
		var rect = rtool.GetMinAreaRect(rtool.GetAllNewRectDatas(v3s));
		return rect;
	}
}

