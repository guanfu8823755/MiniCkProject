using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class MapData
{
	public Dictionary<int, DCity> Cities { get; set; } = new();
	public Dictionary<string, DEdge> Edges { get; set; } = new();
	public Dictionary<int, DPoint> Points { get; set; } = new();
	public bool ContainsEdgeKey(string key)
	{
		return Edges.ContainsKey(key);
	}
	public bool ContainsPointKey(int key)
	{
		return Points.ContainsKey(key);
	}
}
public class DCity
{
	public int Id { get; set; }
	public V3 Pos { get; set; }
	public List<int> Roads { get; set; } = new();
	public List<string> CityEdges { get; set; } = new();
}
public class DEdge
{
	public string key { get; set; }
	public List<int> Points { get; set; } = new();
	public List<int> Cities { get; set; } = new();
	public int StartPoint() { return Points[0]; }
	public int EndPoint() { return Points[Points.Count - 1]; }
}
public class DPoint
{
	public int Id { get; set; }
	public V3 Position { get; set; }
}
public struct V3
{
	public float X { get; set; }
	public float Y { get; set; }
	public float Z { get; set; }

	public V3(Vector3 vector)
	{
		X = vector.x;
		Y = vector.y;
		Z = vector.z;
	}
}


public static class MapTools
{
	public static V3 ToV3(this Vector3 vector)
	{
		return new(vector);
	}
	public static Vector3 ToVector3(this V3 v3)
	{
		return new(v3.X, v3.Y, v3.Z);
	}
	public static string GetKey(this List<int> list)
	{
		var ids = list.Select(i => i).ToList();
		ids.Sort();
		if (ids.Count == 0)
			return string.Empty;
		string key = ids[0].ToString();
		ids.RemoveAt(0);
		if (ids.Count == 0)
			return key;
		for (int i = 0; i < ids.Count; i++)
		{

			key += $"_{ids[i]}";
		}
		return key;
	}


}