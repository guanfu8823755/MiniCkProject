using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LubanTest : MonoBehaviour
{
	public Text text;
	public static LubanTest Instance { get; private set; }
	private void Awake()
	{
		Instance = this;
	}
	// Start is called before the first frame update
	void Start()
	{
		var tables = new cfg.Tables(LoadByteBuf);
		//      cfg.Game.StartConfig startConfig = tables.TbStart.Get(1);
		//cfg.Game.Time time = startConfig.StartTime;
		//      text.text = $"year:{time.Year} month:{time.Mounth} dat:{time.Day}";
		float speed1 = tables.TbConfig.SpeedLevel1;
		float speed2 = tables.TbConfig.SpeedLevel2;
		float speed3 = tables.TbConfig.SpeedLevel3;
		float speed4 = tables.TbConfig.SpeedLevel4;
		float speed5 = tables.TbConfig.SpeedLevel5;
		text.text = $"{speed1},{speed2},{speed3},{speed4},{speed5}";
	}
	private static JSONNode LoadByteBuf(string file)
	{
		string path = "Defines/" + file;
		string fileText = Resources.Load<TextAsset>(path).text;
		JSONNode json = JSON.Parse(fileText);
		//json.ToString().Log();
		return json;

	}
	// Update is called once per frame
	void Update()
	{

	}
}
