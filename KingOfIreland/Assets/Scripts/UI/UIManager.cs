using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FairyGUI;

public class UIManager : MonoBehaviour
{
	public static UIManager Instance;
	private GComponent _mainView;
	private Dictionary<string, GComponent> _uiObjects;
	private void Awake()
	{
		Instance = this;
		UIPackage.AddPackage("FGUI/KOI");
		_uiObjects = new();
	}
	private void Start()
	{
		_mainView = this.GetComponent<UIPanel>().ui;
		InitUI();
	}
	private void InitUI()
	{
		OpenUI("GameMain");

	}

	private void OpenUI(string uiName)
	{
		GComponent ui;
		if(!_uiObjects.TryGetValue(uiName,out ui))
		{
			ui = UIPackage.CreateObject("KOI", uiName).asCom;
			_uiObjects[uiName] = ui;
		}
		_mainView.RemoveChildren();
		_mainView.AddChild(ui);
	}
}
