using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEditor;
using System.Linq;

[ExecuteInEditMode]
public class City : MonoBehaviour
{
	public static bool IsActive = false;
	[SerializeField]
	public int CityId;
	public GameObject text;
	public string CityName;
	public List<Road> Roads = new();
	private Vector3 StartPos = Vector3.zero;
	//public GameObject Canvas;
	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

#if UNITY_EDITOR

		if (StartPos != transform.position)
		{
			StartPos = transform.position;
			Roads.ForEach(e => e.UpdatePoints());
		}

#endif
	}
#if UNITY_EDITOR
	[CustomEditor(typeof(City))]
	public class CityScirptInsector : Editor
	{
		public override void OnInspectorGUI()
		{
			//base.OnInspectorGUI();
			City city = (City)target;
			if (DrawDefaultInspector())
			{
				city.text.GetComponent<TMP_Text>().text = city.CityName;
				city.gameObject.name = $"city_{city.CityId}";
			}

		}

	}

	public class CityEditor
	{

		[MenuItem("GameObject/MapCreator/�����µ�City")]
		public static void NewCity()
		{
			CreateCity();
		}

		public  static City CreateCity()
		{
			Map map = FindFirstObjectByType<Map>();
			if (map == null)
			{
				map = new GameObject("Map").AddComponent<Map>();
				map.transform.position = Vector3.zero;
				map.transform.rotation = Quaternion.identity;
				map.transform.localScale = Vector3.one;
			}
			var prefab = Resources.Load<GameObject>("Prefabs/city");
			var city = Instantiate(prefab).GetComponent<City>();
			city.gameObject.name = "City";
			city.gameObject.SetActive(true);
			city.transform.SetParent(map.transform);

			var c1 = Selection.activeGameObject?.GetComponent<City>();
			if (c1 != null)
			{
				city.transform.localPosition = c1.transform.position;
			}
			else
			{

				city.transform.localPosition = Vector3.zero;

			}

			city.CityId = map.CityId;
			string cityName = city.CityId.ToString();
			city.CityName = cityName;
			city.text.GetComponent<TMP_Text>().text = cityName;
			city.transform.Find("Canvas").hideFlags = HideFlags.HideInHierarchy;
			EditorGUIUtility.PingObject(city);
			Selection.activeObject = city;
			return city;
		}
	}
#endif
}
