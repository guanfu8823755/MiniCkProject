using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[ExecuteInEditMode, RequireComponent(typeof(LineRenderer))]
public class Edge : MonoBehaviour
{
	public List<EdgePoint> Points = new();
	public List<int> MyCityIds = new();
	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}
	private void OnEnable()
	{
		GetComponent<LineRenderer>().startWidth = 0.05f;
		GetComponent<LineRenderer>().endWidth = 0.05f;
	}

	public void UpdatePoints()
	{
		var line = GetComponent<LineRenderer>();
		line.SetPositions(Points.Select(p => p.transform.position).ToArray());
	}
	public void SetPoints(List<EdgePoint> points)
	{
		Points = points;
		if (Points.Count == 2)
		{
			var names = Points.Select(p => p.name).ToList();
			this.gameObject.name = GetName(names);
		}
		UpdateRender();
	}
	public void UpdateRender()
	{
		var line = GetComponent<LineRenderer>();
		var mat = Resources.Load<Material>("Materials/edge");
		line.sharedMaterial = mat;
		line.SetPositions(Points.Select(p => p.transform.position).ToArray());
	}
	public static string GetName(List<string> names)
	{
		names.Sort();
		return $"{names[0]}__{names[1]}";
	}
}
