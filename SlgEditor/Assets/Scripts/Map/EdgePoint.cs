using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
[ExecuteInEditMode]
public class EdgePoint : MonoBehaviour
{
	public List<Edge> edges = new List<Edge>();
	private Vector3 StartPos = Vector3.zero;
	public bool IsDefaultPoint = true;
	public List<int> MyCityIds = new();

	// Start is called before the first frame update
	void Start()
	{

	}
	public void AddEdge(Edge edge)
	{
		if (!edges.Contains(edge))
		{
			edges.Add(edge);
		}
	}
	// Update is called once per frame
	void Update()
	{
#if UNITY_EDITOR

		if (StartPos != transform.position)
		{
			StartPos = transform.position;
			edges.ForEach(e => e.UpdatePoints());
		}

#endif
	}

}
