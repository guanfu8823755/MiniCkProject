using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System;
using YamlDotNet.Serialization;
using System.IO;

[ExecuteInEditMode]
public class Map : MonoBehaviour
{
	public int cityId;
	public EdgePoint edgePoint;
	public PolygonMesh polygon;
	public int CityId { get { return cityId++; } }
	protected SceneView scene;
	// Start is called before the first frame update
	void Start()
	{

	}
	private void OnEnable()
	{
		SceneView.duringSceneGui += OnScene;
	}
	// Update is called once per frame
	void Update()
	{

	}
	public void OnScene(SceneView scene)
	{
		if (this == null)
		{
			print("deleted Map, removing listener...");
			SceneView.duringSceneGui -= OnScene;
		}
		this.scene = scene;
		Event e = Event.current;
		//创建道路
		if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Tab)
		{
			var cities = Selection.gameObjects.Where(x => x.TryGetComponent(out City city)).Select(x => x.GetComponent<City>()).ToList();
			if (cities.Count == 2)
			{
				Roads edges = FindFirstObjectByType<Roads>();
				if (edges == null)
				{
					edges = new GameObject("Roads").AddComponent<Roads>();
					edges.transform.position = Vector3.zero;
					edges.transform.rotation = Quaternion.identity;
					edges.transform.localScale = Vector3.one;
					edges.gameObject.SetActive(true);
				}
				int cityId_1 = cities[0].CityId;
				int cityId_2 = cities[1].CityId;
				string EdgeName = cityId_1 < cityId_2 ? $"{cityId_1}_{cityId_2}" : $"{cityId_2}_{cityId_1}";
				var go = edges.transform.Find(EdgeName);
				if (go == null)
				{
					var road = new GameObject(EdgeName).AddComponent<Road>();
					road.CititesAddRange(cities);
					road.gameObject.SetActive(true);
					road.transform.SetParent(edges.transform);
					cities.ForEach(c => c.Roads.Add(road));
					Selection.activeGameObject = road.gameObject;
				}
				else
				{
					Debug.LogError("已存在对应边，不要重复创建");
				}
			}
		}
		//删除道路
		if (e.type == EventType.KeyDown && e.keyCode == KeyCode.D)
		{
			var edges = Selection.gameObjects.Where(x => x.TryGetComponent(out Road city)).Select(x => x.GetComponent<Road>()).ToList();
			if (edges.Count > 0)
			{
				edges.ForEach(e => { e.Cities.ForEach(c => c.Roads.Remove(e)); });
				for (int i = edges.Count - 1; i >= 0; i--)
				{
					DestroyImmediate(edges[i].gameObject);
				}
			}
		}
		//生成边界
		if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Alpha1)
		{
			//创建边界点父节点
			var edgePoints = FindFirstObjectByType<EdgePoints>();
			if (edgePoints == null)
			{
				GameObject go = new GameObject("EdgePoints");
				go.transform.position = new Vector3(0, 0, 0.2f);
				go.transform.rotation = Quaternion.identity;
				go.transform.localScale = Vector3.one;
				edgePoints = go.AddComponent<EdgePoints>();
			}
			//清理现有的边界点
			var pointObjects = FindObjectsOfType<EdgePoint>();
			for (int i = pointObjects.Length - 1; i >= 0; i--)
			{
				DestroyImmediate(pointObjects[i].gameObject);
			}

			//创建边界父节点
			var edges = FindFirstObjectByType<Edges>();
			if (edges == null)
			{
				GameObject go = new GameObject("Edges");
				go.transform.position = new Vector3(0, 0, 0.1f);
				go.transform.rotation = Quaternion.identity;
				go.transform.localScale = Vector3.one;
				edges = go.AddComponent<Edges>();
			}
			//清理现有边界
			var edgeObjects = FindObjectsOfType<Edge>();
			for (int i = edgeObjects.Length - 1; i >= 0; i--)
			{
				DestroyImmediate(edgeObjects[i].gameObject);
			}


			var Points = FindObjectsOfType<City>().ToList();
			//道路表cityId->cityIds
			Dictionary<int, List<int>> Roads = new();
			//城市表
			Dictionary<int, City> Cities = new();
			//三角形边
			Dictionary<string, EdgePoint> TriangleCenters = new();
			//边界表
			Dictionary<string, Edge> EdgeDic = new();
			Points.ForEach(c =>
			{
				Cities.Add(c.CityId, c);
				c.Roads.ForEach(e =>
				{
					string rel = e.gameObject.name;
					var points = rel.Split("_");
					if (points.Length == 2)
					{
						int p1 = int.Parse(points[0]);
						int p2 = int.Parse(points[1]);
						if (p1 == c.CityId)
						{
							Roads.Add(c.CityId, p2);
						}
						else
						{
							Roads.Add(c.CityId, p1);
						}
					}
				});
			});

			var TrueCityIds = Cities.Keys.Where(id => id < 1000).ToList();
			Debug.Log(Cities.Count + "   " + TrueCityIds.Count);


			foreach (var CityId in TrueCityIds)
			{
				//int CityId = 2;
				if (Cities.TryGetValue(CityId, out City theCity))
				{
					Vector3 cityPos = theCity.transform.position;
					var rs = Roads[CityId];
					List<(int, double)> cityPositions = new();//周边城市和该城市连线的向量角度
					rs.ForEach(id =>
					{
						Vector3 pos = Cities[id].transform.position;
						var vector = pos - cityPos;
						Vector2 v2 = new Vector2(vector.x, vector.y);
						var angle = Math.Atan2(v2.y, v2.x);
						cityPositions.Add((id, angle));
					});

					cityPositions.Sort((a, b) => a.Item2.CompareTo(b.Item2));//将城市沿着顺/逆时针排序

					List<string> epoints = new();
					for (int i = 0; i < cityPositions.Count; i++)
					{
						List<int> cts = new();
						cts.Add(CityId);
						cts.Add(cityPositions[i].Item1);
						int thirdCityId = i + 1;
						thirdCityId %= cityPositions.Count;
						cts.Add(cityPositions[thirdCityId].Item1);
						cts.Sort();
						string key = $"{cts[0]}_{cts[1]}_{cts[2]}";
						if (!TriangleCenters.TryGetValue(key, out EdgePoint center))
						{
							var vector3s = cts.Select(c => Cities[c].transform.position).ToList();
							Vector3 pos = (vector3s[0] + vector3s[1] + vector3s[2]) / 3f;
							center = Instantiate(edgePoint);
							center.gameObject.name = key;
							center.transform.position = pos;
							center.transform.SetParent(edgePoints.transform);
							center.MyCityIds = cts;
							TriangleCenters.Add(key, center);
						}
						epoints.Add(key);
					}
					for (int i = 0; i < epoints.Count; i++)
					{
						string p1 = epoints[i];
						int p2n = i + 1;
						p2n %= epoints.Count;
						string p2 = epoints[p2n];
						List<string> name = new() { p1, p2 };
						string edgeName = Edge.GetName(name);
						if (EdgeDic.TryGetValue(edgeName, out var edge))
						{
						}
						else
						{
							edge = new GameObject().AddComponent<Edge>();
							edge.SetPoints(name.Select(key => TriangleCenters[key]).ToList());
							EdgeDic.Add(edgeName, edge);
						}
						edge.MyCityIds.Add(CityId);

						name.ForEach(key =>
						{
							TriangleCenters[key].AddEdge(edge);
						});
						edge.transform.SetParent(edges.transform);
					}

					//string log = $"Order {theCity.name}:";
					//cityPositions.ForEach(i => { log += $" {i.Item1},"; });
					////{ i.Item2} 
					//Debug.Log(log);



				}
			}

		}
		if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Alpha3)
		{
			var edges = Selection.gameObjects.Where(x => x.TryGetComponent(out Edge edge)).Select(x => x.GetComponent<Edge>()).ToList();
			if (edges.Count > 0)
			{
				edges.ForEach(e =>
				{
					//拿到点的父物体
					var edgePoints = FindFirstObjectByType<EdgePoints>();
					//新建点
					var newPoint = Instantiate(edgePoint);
					newPoint.gameObject.name = "middle_point";
					newPoint.transform.SetParent(edgePoints.transform, false);
					newPoint.transform.position = (e.Points[0].transform.position + e.Points[1].transform.position) / 2f;
					newPoint.IsDefaultPoint = false;
					newPoint.MyCityIds = e.MyCityIds;
					//拿到边的父物体
					var edges = FindFirstObjectByType<Edges>();
					//拿到该边的两个顶点
					var points = e.Points;
					var p1 = points[0];
					var p2 = points[1];

					var newEdge = new GameObject().AddComponent<Edge>();
					newEdge.transform.SetParent(edges.transform, false);
					newEdge.SetPoints(new() { p2, newPoint });
					newEdge.MyCityIds = e.MyCityIds;

					newPoint.AddEdge(newEdge);
					p2.edges.Remove(e);
					p2.AddEdge(newEdge);

					e.SetPoints(new() { p1, newPoint });
					newPoint.AddEdge(e);
					Selection.activeGameObject = newPoint.gameObject;
				});

			}
		}
		if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Alpha4)
		{
			var points = Selection.gameObjects.Where(x => x.TryGetComponent(out EdgePoint point)).Select(x => x.GetComponent<EdgePoint>()).ToList();
			if (points.Count > 0)
			{
				points.ForEach(p =>
				{
					if (!p.IsDefaultPoint)
					{
						var edges = p.edges;
						List<EdgePoint> newPoints = new();
						edges.ForEach(e =>
						{
							e.Points.ForEach(thePoint =>
							{
								if (!thePoint.Equals(p))
								{
									newPoints.Add(thePoint);
								}
							});
						});
						var te1 = edges[1];
						var te2 = edges[0];
						te1.Points.ForEach(p1 =>
						{
							p1.edges.Remove(te1);
						});
						DestroyImmediate(te1.gameObject);
						te2.SetPoints(newPoints);
						newPoints.ForEach(p2 =>
						{
							p2.AddEdge(te2);
						});
						DestroyImmediate(p.gameObject);
					}
				});
			}
		}
		if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Alpha5)
		{
			var PolygonMeshes = FindObjectsOfType<PolygonMesh>();
			foreach (var polygonMesh in PolygonMeshes)
			{
				DestroyImmediate(polygonMesh.gameObject);
			}

			MapData data = new MapData();
			var cities = FindObjectsOfType<City>().ToList();
			cities.ForEach(c =>
			{
				if (c.CityId < 1000)
				{
					DCity theCity = new();
					theCity.Id = c.CityId;
					theCity.Pos = c.transform.position.ToV3();
					data.Cities.Add(theCity.Id, theCity);
				}
			});

			var roads = FindObjectsOfType<Road>().ToList();
			roads.ForEach(r =>
			{
				var road = r;
				Debug.Log(444);
				var cts = road.Cities.Select(c => c.CityId).ToList();
				if (cts[0] < 1000 && cts[1] < 1000)
				{
					data.Cities[cts[0]].Roads.Add(cts[1]);
					data.Cities[cts[1]].Roads.Add(cts[0]);
				}
			});

			var edges = FindObjectsOfType<Edge>().ToList();
			edges.ForEach(e =>
			{
				string key = e.MyCityIds.GetKey();
				if (!data.ContainsEdgeKey(key))
				{
					if (e.CanBeFirstEdge(out int id))
					{
						DEdge dEdge = new();
						dEdge.key = key;
						dEdge.Cities = e.MyCityIds;
						foreach (var cid in dEdge.Cities)
						{
							data.Cities[cid].CityEdges.Add(key);
						}
						int PointId = e.Points[id].GetInstanceID();
						if (!data.ContainsPointKey(PointId))
						{
							DPoint dPoint = new();
							dPoint.Position = e.Points[id].transform.position.ToV3();
							dPoint.Id = PointId;
							data.Points.Add(PointId, dPoint);
						}
						//						var firstPoint = data.Points[PointId];
						dEdge.Points.Add(PointId);
						var id2 = 1 - id;
						var otherPoint = e.Points[id2];
						int edgeId = e.GetInstanceID();
						var OtherEdge = e;
						while ((!otherPoint.IsDefaultPoint) || (otherPoint.MyCityIds.Where(id => id < 1000).Count() == 1))
						{
							Debug.Log(333);
							OtherEdge = otherPoint.GetAnotherEdge(OtherEdge.GetInstanceID());
							var otherEdgeId = OtherEdge.GetInstanceID();
							PointId = otherPoint.GetInstanceID();
							if (!data.ContainsPointKey(PointId))
							{
								DPoint dPoint = new();
								dPoint.Position = otherPoint.transform.position.ToV3();
								dPoint.Id = PointId;
								data.Points.Add(PointId, dPoint);
							}
							dEdge.Points.Add(PointId);
							otherPoint = OtherEdge.GetAnotherPoint(PointId);
						}
						PointId = otherPoint.GetInstanceID();
						if (!data.ContainsPointKey(PointId))
						{
							DPoint dPoint = new();
							dPoint.Position = otherPoint.transform.position.ToV3();
							dPoint.Id = PointId;
							data.Points.Add(PointId, dPoint);
						}
						dEdge.Points.Add(PointId);
						data.Edges.Add(key, dEdge);
					}
				}
			});

			Serializer serializer = new();
			string yaml = serializer.Serialize(data);

			string path = Path.Combine(Application.dataPath, "Map.txt");
			if (File.Exists(path))
			{
				File.Delete(path);
			}
			File.WriteAllText(path, yaml);


		}

		if (e.type == EventType.KeyDown && e.keyCode == KeyCode.Alpha6)
		{
			string path = Path.Combine(Application.dataPath, "Map.txt");
			string file = File.ReadAllText(path);
			Deserializer deserializer = new Deserializer();

			MapData data = deserializer.Deserialize<MapData>(file);
			foreach (var kp in data.Cities)
			{
				List<Vector3> points = new();

				var city = kp.Value;
				var cityEdges = city.CityEdges.Select(v => data.Edges[v]).ToList();
				var firstEdge = cityEdges[0];
				cityEdges.Remove(firstEdge);
				var startPoint = firstEdge.StartPoint();
				for (int i = 0; i < firstEdge.Points.Count - 1; i++)
				{
					points.Add(data.Points[firstEdge.Points[i]].Position.ToVector3());
				}

				var endPoint = firstEdge.EndPoint();

				while (startPoint != endPoint)
				{

					Debug.Log(222);
					firstEdge = cityEdges.First(ce => ce.StartPoint() == endPoint || ce.EndPoint() == endPoint);
					cityEdges.Remove(firstEdge);
					if (firstEdge == null)
					{
						Debug.LogError("错误！！！！");
					}
					if (firstEdge.StartPoint() == endPoint)
					{
						endPoint = firstEdge.EndPoint();
						for (int i = 0; i < firstEdge.Points.Count - 1; i++)
						{
							points.Add(data.Points[firstEdge.Points[i]].Position.ToVector3());
						}
					}
					else
					{
						endPoint = firstEdge.StartPoint();
						for (int i = firstEdge.Points.Count - 1; i > 0; i--)
						{
							points.Add(data.Points[firstEdge.Points[i]].Position.ToVector3());
						}
					}
				}
				var pmesh = new GameObject($"city_{kp.Key}").AddComponent<PolygonMesh>();
				pmesh.transform.position = Vector3.zero;
				pmesh.transform.rotation = Quaternion.identity;
				pmesh.transform.localScale = Vector3.one;
				pmesh.DrawMeshOfList(points);
			}
		}
		if (e.type == EventType.KeyDown && e.keyCode == KeyCode.V)
		{
			var cities = Selection.gameObjects.Where(x => x.TryGetComponent(out City city)).Select(x => x.GetComponent<City>()).ToList();
			if (cities.Count > 0)
			{
				var c0 = cities[0];
				var city = City.CityEditor.CreateCity();
				city.transform.position = c0.transform.localPosition;
			}
		}
	}
}

public static class MapTools
{
	public static V3 ToV3(this Vector3 vector)
	{
		return new(vector);
	}
	public static Vector3 ToVector3(this V3 v3)
	{
		return new(v3.X, v3.Y, v3.Z);
	}
	public static string GetKey(this List<int> list)
	{
		var ids = list.Select(i => i).ToList();
		ids.Sort();
		if (ids.Count == 0)
			return string.Empty;
		string key = ids[0].ToString();
		ids.RemoveAt(0);
		if (ids.Count == 0)
			return key;
		for (int i = 0; i < ids.Count; i++)
		{

			key += $"_{ids[i]}";
		}
		return key;
	}

	public static bool CanBeFirstEdge(this Edge edge, out int id)
	{
		for (int i = 0; i < edge.Points.Count; i++)
		{
			id = i;
			if (edge.Points[i].IsDefaultPoint && (edge.Points[i].MyCityIds.Where(id => id < 1000).Count() > 1))
			{
				return true;
			}
		}
		id = -1;
		return false;
	}

	public static Edge GetAnotherEdge(this EdgePoint point, int edgeId)
	{
		foreach (var edge in point.edges)
		{
			if (edge.GetInstanceID() != edgeId)
			{
				return edge;
			}
		}
		return null;
	}
	public static EdgePoint GetAnotherPoint(this Edge edge, int pointId)
	{
		foreach (var point in edge.Points)
		{
			if (point.GetInstanceID() != pointId)
			{
				return point;
			}
		}
		return null;
	}
}
public class MapData
{
	public Dictionary<int, DCity> Cities { get; set; } = new();
	public Dictionary<string, DEdge> Edges { get; set; } = new();
	public Dictionary<int, DPoint> Points { get; set; } = new();
	public bool ContainsEdgeKey(string key)
	{
		return Edges.ContainsKey(key);
	}
	public bool ContainsPointKey(int key)
	{
		return Points.ContainsKey(key);
	}
}
public class DCity
{
	public int Id { get; set; }
	public V3 Pos { get; set; }
	public List<int> Roads { get; set; } = new();
	public List<string> CityEdges { get; set; } = new();
}
public class DEdge
{
	public string key { get; set; }
	public List<int> Points { get; set; } = new();
	public List<int> Cities { get; set; } = new();
	public int StartPoint() { return Points[0]; }
	public int EndPoint() { return Points[Points.Count - 1]; }
}
public class DPoint
{
	public int Id { get; set; }
	public V3 Position { get; set; }
}
public struct V3
{
	public float X { get; set; }
	public float Y { get; set; }
	public float Z { get; set; }

	public V3(Vector3 vector)
	{
		X = vector.x;
		Y = vector.y;
		Z = vector.z;
	}
}