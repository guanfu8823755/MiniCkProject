using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using YamlDotNet.Serialization;

public class MapShow : MonoBehaviour
{
	public MapData data;
	public TextMeshPro text;

	private Vector3 height = new Vector3(0, 0, -0.1f);
	private Dictionary<int, PolygonMesh> CityMeshes = new();
	private Dictionary<int, List<int>> GongGuo = new()
	{
		{1,new(){1,2,3,4} },
		{2,new(){5,6,7,8} },
	};
	private Dictionary<int, string> CityNames = new()
	{
		{1,"��˹�ɵ�" },
		{2,"���ɵ�" }
	};
	private RectangleTool rtool = new();

	private void Awake()
	{
		string path = Path.Combine(Application.dataPath, "Map.txt");
		string file = File.ReadAllText(path);
		Deserializer deserializer = new Deserializer();

		data = deserializer.Deserialize<MapData>(file);

	}
	// Start is called before the first frame update
	void Start()
	{
		CreateCities();

		HashSet<string> edgesToDraw = new();
		foreach (var Area in GongGuo)
		{
			Color color = Random.ColorHSV();

			HashSet<string> edgesAll = new();
			HashSet<string> repeatingEdges = new();
			HashSet<int> Points = new();


			foreach (int cityid in Area.Value)
			{
				CityMeshes[cityid].SetColor(color);
				var dc = data.Cities[cityid];
				dc.CityEdges.ForEach(edge =>
				{
					if (!edgesAll.Add(edge))
					{
						repeatingEdges.Add(edge);
					}
				});

			}

			var edges = edgesAll.Except(repeatingEdges);
			edgesToDraw.UnionWith(edges);

			var rect = GetRectangle(Area.Value);
			Debug.Log($"{Area.Key},{rect.theta}");
			Debug.Log($"{Area.Key},{rect.center}");
			Debug.Log($"{Area.Key},{rect.boundary[0]},{rect.boundary[1]},{rect.boundary[2]},{rect.boundary[3]}");

			//CreateCity(Area.Key.ToString(), rect.boundary);

			float width = Vector3.Distance(rect.boundary[0], rect.boundary[1]);
			float height = Vector3.Distance(rect.boundary[1], rect.boundary[2]);
			var title = Instantiate(text);
			title.text = CityNames[Area.Key];
			title.gameObject.name = Area.Key.ToString();
			int angle = 0;
			if (width > height)
			{
				title.rectTransform.sizeDelta = new Vector2(width, height);
			}
			else
			{
				title.rectTransform.sizeDelta = new Vector2(height, width);
				angle = 90;
			}

			title.rectTransform.position = (rect.boundary[0] + rect.boundary[1] + rect.boundary[2] + rect.boundary[3]) / 4f;
			title.rectTransform.localEulerAngles = new Vector3(0, 0, -rect.theta + angle);
			title.rectTransform.localScale = new Vector2(0.6f, 0.6f);
		}
		CreateEdgesByEdgeId(edgesToDraw);


		//CreateRoads();
		//CreateEdges();
		//CreateRectangle();
	}
	private RectangleTool.RectData GetRectangle(List<int> cityIds)
	{
		var cities = cityIds.Select(cid => data.Cities[cid]).ToList();

		var v3s = cities.SelectMany(c => c.CityEdges).SelectMany(e => data.Edges[e].Points).Select(p => data.Points[p].Position.ToVector3()).ToList();
		var rect = rtool.GetMinAreaRect(rtool.GetAllNewRectDatas(v3s));
		return rect;
		//var pmesh = new GameObject($"rect").AddComponent<PolygonMesh>();
		//pmesh.transform.position = Vector3.zero;
		//pmesh.transform.rotation = Quaternion.identity;
		//pmesh.transform.localScale = Vector3.one;
		//pmesh.DrawMeshOfList(rect.boundary);
	}

	private void CreateCities()
	{
		foreach (var kp in data.Cities)
		{
			List<Vector3> points = new();

			var city = kp.Value;
			var cityEdges = city.CityEdges.Select(v => data.Edges[v]).ToList();
			var firstEdge = cityEdges[0];
			cityEdges.Remove(firstEdge);
			var startPoint = firstEdge.StartPoint();
			for (int i = 0; i < firstEdge.Points.Count - 1; i++)
			{
				points.Add(data.Points[firstEdge.Points[i]].Position.ToVector3());
			}

			var endPoint = firstEdge.EndPoint();

			while (startPoint != endPoint)
			{

				Debug.Log(222);
				firstEdge = cityEdges.First(ce => ce.StartPoint() == endPoint || ce.EndPoint() == endPoint);
				cityEdges.Remove(firstEdge);
				if (firstEdge == null)
				{
					Debug.LogError("���󣡣�����");
				}
				if (firstEdge.StartPoint() == endPoint)
				{
					endPoint = firstEdge.EndPoint();
					for (int i = 0; i < firstEdge.Points.Count - 1; i++)
					{
						points.Add(data.Points[firstEdge.Points[i]].Position.ToVector3());
					}
				}
				else
				{
					endPoint = firstEdge.StartPoint();
					for (int i = firstEdge.Points.Count - 1; i > 0; i--)
					{
						points.Add(data.Points[firstEdge.Points[i]].Position.ToVector3());
					}
				}
			}
			var City = CreateCity($"city_{kp.Key}", points);
			CityMeshes.Add(kp.Key, City);
		}
	}

	private PolygonMesh CreateCity(string Name, List<Vector3> Points)
	{
		//		var pmesh = new GameObject($"city_{kp.Key}").AddComponent<PolygonMesh>();
		var pmesh = new GameObject(Name).AddComponent<PolygonMesh>();
		pmesh.transform.position = Vector3.zero;
		pmesh.transform.rotation = Quaternion.identity;
		pmesh.transform.localScale = Vector3.one;
		pmesh.DrawMeshOfList(Points);
		return pmesh;
	}
	private void CreateRoads()
	{
		HashSet<string> roads = new();
		foreach (var kp in data.Cities)
		{
			var city = kp.Value;
			foreach (var city2 in city.Roads)
			{
				if (city2 > city.Id)
				{
					var Line = new GameObject($"{city.Id}_{city2}").AddComponent<LineRenderer>();
					Line.sharedMaterial = Resources.Load<Material>("Materials/road");
					Line.startWidth = 0.05f;
					Line.endWidth = 0.05f;
					Line.positionCount = 2;
					var list = new List<int>() { city2, city.Id };
					Line.SetPositions(list.Select(i => data.Cities[i].Pos.ToVector3() + 2 * height).ToArray());
				}
			}
		}
	}

	private void CreateEdges()
	{
		data.Edges.Keys.ToList().ForEach(e =>
		{
			var ps = data.Edges[e].Points.Select(pid => data.Points[pid].Position.ToVector3() + height).ToArray();
			var edge = new GameObject(e).AddComponent<LineRenderer>();
			edge.sharedMaterial = Resources.Load<Material>("Materials/edge");
			edge.startWidth = 0.1f;
			edge.endWidth = 0.1f;
			edge.positionCount = ps.Length;
			edge.SetPositions(ps);
		});
	}

	private void CreateEdgesByEdgeId(HashSet<string> ids)
	{
		ids.ToList().ForEach(e =>
		{
			var ps = data.Edges[e].Points.Select(pid => data.Points[pid].Position.ToVector3() + height).ToArray();
			var edge = new GameObject(e).AddComponent<LineRenderer>();
			edge.sharedMaterial = Resources.Load<Material>("Materials/edge");
			edge.startWidth = 0.1f;
			edge.endWidth = 0.1f;
			edge.positionCount = ps.Length;
			edge.SetPositions(ps);
		});
	}

	// Update is called once per frame
	void Update()
	{

	}
}
