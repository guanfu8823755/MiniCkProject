using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class MiddlePoint : MonoBehaviour
{
    Vector3 startPoint= Vector3.zero;
    public Road edge;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(startPoint != transform.position)
		{
            startPoint = transform.position;
            edge?.UpdatePoints();
		}
    }
}
