using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using System.Reflection;


[ExecuteInEditMode, RequireComponent(typeof(LineRenderer))]
public class Road : MonoBehaviour
{
	public List<City> Cities = new List<City>();
	public bool HasMiddlePoint = false;
	public GameObject MiddlePoint;
	// Start is called before the first frame update
	void Start()
	{

	}
	private void OnEnable()
	{
		GetComponent<LineRenderer>().startWidth = 0.1f;
		GetComponent<LineRenderer>().endWidth = 0.1f;
	}
	public void CititesAddRange(List<City> cs)
	{
		Cities.AddRange(cs);
		UpdateRender();
	}


	public void UpdateRender()
	{
		var line = GetComponent<LineRenderer>();
		var mat = Resources.Load<Material>("Materials/road");
		line.sharedMaterial = mat;
		line.SetPositions(Cities.Select(c => c.transform.position).ToArray());
	}
	// Update is called once per frame
	void Update()
	{

	}
	public void UpdatePoints()
	{
		var line = GetComponent<LineRenderer>();
		if (HasMiddlePoint)
		{
			var positions = Cities.Select(c => c.transform.position).ToList();
			positions.Insert(1, MiddlePoint.transform.position);
			Debug.Log($"pos num:{positions.Count}");
			line.positionCount= positions.Count;
			line.SetPositions(positions.ToArray());
		}
		else
		{
			line.SetPositions(Cities.Select(c => c.transform.position).ToArray());
		}
	}
}
