﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class RectangleTool
{
	// 1.寻找多边形的中心 
	public Vector3 GetCenter(List<Vector3> pts)
	{
		float sumx = 0;
		float sumy = 0;
		foreach (var p in pts)
		{
			sumx = sumx + p.x;
			sumy = sumy + p.y;
		}
		var pt = new Vector3(sumx / pts.Count, sumy / pts.Count, 0);
		return pt;
	}

	// 2.旋转多边形，针对每个点实现绕中心点旋转

	public Vector3 RotatePt(Vector3 inpt, Vector3 centerPt, float theta)
	{
		float ix = inpt.x;
		float iy = inpt.y;
		float cx = centerPt.x;
		float cy = centerPt.y;
		float Q = theta / 180 * 3.1415926f;  //角度

		float ox, oy;
		ox = (float)((ix - cx) * Math.Cos(Q) - (iy - cy) * Math.Sin(Q) + cx);   //旋转公式
		oy = (float)((ix - cx) * Math.Sin(Q) + (iy - cy) * Math.Cos(Q) + cy);

		var outpt = new Vector3(ox, oy, 0);
		return outpt;
	}

	// 3.多边形旋转后求简单外接矩形

	public List<Vector3> GetRect(List<Vector3> inpts)
	{
		var outpts = new List<Vector3>();
		int size = inpts.Count;
		if (size == 0)
			return null;
		else
		{
			var tempx = new List<float>();
			var tempy = new List<float>();
			for (int i = 0; i < size; i++)
			{
				tempx.Add(inpts[i].x);
				tempy.Add(inpts[i].y);
			}

			Vector3 endpoint0 = new Vector3(tempx.Min(), tempy.Max(), 0);
			Vector3 endpoint1 = new Vector3(tempx.Max(), tempy.Max(), 0);
			Vector3 endpoint2 = new Vector3(tempx.Max(), tempy.Min(), 0);
			Vector3 endpoint3 = new Vector3(tempx.Min(), tempy.Min(), 0);
			outpts.Add(endpoint0);
			outpts.Add(endpoint1);
			outpts.Add(endpoint2);
			outpts.Add(endpoint3);
			return outpts;
		}
	}
	// 4.存储每个旋转角度下多边形的外接矩形，记录外接矩形的顶点坐标、面积和此时多边形的旋转角度

	public class RectData
	{
		public List<Vector3> boundary { get; set; }
		public Vector3 center { get; set; }
		public float theta { get; set; }
		public float area { get; set; }

	}

	public RectData GetRotateRectDatas(List<Vector3> inpts, float theta)
	{

		Vector3 center = GetCenter(inpts);
		var tempvertices = new List<Vector3>();
		for (int i = 0; i < inpts.Count; i++)
		{
			Vector3 temp = RotatePt(inpts[i], center, theta);
			tempvertices.Add(temp);
		}
		List<Vector3> vertices = GetRect(tempvertices);
		float deltaX, deltaY;                     //求每个外接矩形的面积
		deltaX = vertices[0].x - vertices[2].x;
		deltaY = vertices[0].y - vertices[2].y;

		var polygen = new RectData
		{
			area = Math.Abs(deltaY * deltaX),
			center = center,
			theta = theta,
			boundary = vertices
		};
		return polygen;
	}

	//获取所有新的矩形
	public List<RectData> GetAllNewRectDatas(List<Vector3> inpts)
	{
		var polygens = new List<RectData>();

		for (int theta = 0; theta <= 90;)
		{
			polygens.Add(GetRotateRectDatas(inpts, theta));
			theta = theta + 5;
		}
		return polygens;
	}
	//获取新的矩形
	public RectData GetMinAreaRect(List<RectData> polygons)
	{

		float minarea = 100000000;
		int N = 0;
		for (int i = 0; i < polygons.Count(); i++)
		{
			if (minarea > polygons[i].area)
			{
				minarea = polygons[i].area;
				N = i;
			}
		}
		var polygon = new RectData();
		polygon = polygons[N];

		//旋转到最小面积的方向
		Vector3 centerPt = GetCenter(polygon.boundary);
		var boundary = new List<Vector3>();
		foreach (var bound in polygon.boundary)
		{
			Vector3 pt = RotatePt(bound, polygon.center, -polygon.theta);
			boundary.Add(pt);
		}
		var outpolygon = new RectData
		{
			center = polygon.center,
			area = polygon.area,
			theta = polygon.theta,
			boundary = boundary
		};
		return outpolygon;
	}
}