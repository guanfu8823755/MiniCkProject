﻿//using System;
//using System.Collections.Generic;

//public class Point
//{
//	public double X { get; set; }
//	public double Y { get; set; }
//}

//public class RotatingCalipers
//{
//	public static double Distance(Point p1, Point p2)
//	{
//		double dx = p1.X - p2.X;
//		double dy = p1.Y - p2.Y;
//		return Math.Sqrt(dx * dx + dy * dy);
//	}

//	public static double DotProduct(Point v1, Point v2)
//	{
//		return v1.X * v2.X + v1.Y * v2.Y;
//	}

//	public static double AngleBetween(Point v1, Point v2)
//	{
//		double dotProduct = DotProduct(v1, v2);
//		double cosAngle = dotProduct


//	/ (Distance(v1, new Point { X = 0, Y = 0 }) * Distance(v2, new Point { X = 0, Y = 0 }));
//		return Math.Acos(cosAngle);
//	}

//	public static List<Point> ConvexHull(List<Point> points)
//	{
//		points.Sort((p1, p2) => p1.X == p2.X ? p1.Y.CompareTo(p2.Y) : p1.X.CompareTo(p2.X));

//		List<Point> hull = new List<Point>();

//		for (int i = 0; i < 2; i++)
//		{
//			int start = hull.Count;
//			foreach (Point p in points)
//			{
//				while (hull.Count >= start + 2 && CrossProduct(hull[hull.Count - 2], hull[hull.Count - 1], p) <= 0)
//				{
//					hull.RemoveAt(hull.Count - 1);
//				}
//				hull.Add(p);
//			}
//			hull.RemoveAt(hull.Count - 1);
//			points.Reverse();
//		}

//		return hull;
//	}

//	public static double CrossProduct(Point p1, Point p2, Point p3)
//	{
//		return (p2.X - p1.X) * (p3.Y - p1.Y) - (p2.Y - p1.Y) * (p3.X - p1.X);
//	}

//	public static double FindMinimumBoundingRectangle(List<Point> points, out Point center, out double angle)
//	{
//		List<Point> hull = ConvexHull(points);

//		double minDiagonal = double.MaxValue;
//		center = new Point { X = 0, Y = 0 };
//		angle = 0;

//		for (int i = 0; i < hull.Count; i++)
//		{
//			int j = (i + 1) % hull.Count;
//			Point v1 = new Point { X = hull[j].X - hull[i].X, Y = hull[j].Y - hull[i].Y };

//			double maxDistance = 0;
//			int maxIndex = 0;

//			for (int k = 0; k < hull.Count; k++)
//			{
//				double distance = CrossProduct(v1, new Point { X = hull[k].X - hull[i].X, Y = hull[k].Y - hull[i].Y });
//				if (distance > maxDistance)
//				{
//					maxDistance = distance;
//					maxIndex = k;
//				}
//			}

//			Point v2 = new Point { X = hull[maxIndex].X - hull[i].X, Y = hull[maxIndex].Y - hull[i].Y };

//			double diagonal = Distance(hull[i], hull[maxIndex]);

//			if (diagonal < minDiagonal)
//			{
//				minDiagonal = diagonal;
//				center.X = (hull[i].X + hull[maxIndex].X) / 2;
//				center.Y = (hull[i].Y + hull[maxIndex].Y) / 2;
//				angle = AngleBetween(new Point { X = 1, Y = 0 }, v2) * 180 / Math.PI;
//			}
//		}

//		return minDiagonal;
//	}
//}

