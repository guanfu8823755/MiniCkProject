using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class TextureTest : MonoBehaviour
{
	public Texture2D texture;
	public int[,] Points;
	public Dictionary<Vector2Int, MPoint> MiddlePoints = new();
	public int Width;
	public int Height;


	// Start is called before the first frame update
	void Start()
	{
		Width = texture.width;
		Height = texture.height;
		Points = new int[Width, Height];
		Dictionary<Vector3Int, int> colors = new();
		Dictionary<Vector3Int, int> colorIds = new();
		int id = 1;
		for (int w = 0; w < Width; w++)
		{
			for (int h = 0; h < Height; h++)
			{
				Color32 pixel = texture.GetPixel(w, h);
				Vector3Int color = new(pixel.r, pixel.g, pixel.b);
				bool contain = false;
				if(w==158 && h==246)
				{
					Debug.Log(color);
				}
				foreach (var c in colors.Keys.ToList())
				{
					if (!contain)
					{
						if (Vector3Int.Distance(c, color) < 5)
						{
							contain = true;
							colors[c]++;
							color = c;
							Points[w, h] = colorIds[color];
						}
					}
				}

				if (!contain)
				{
					colors[color] = 1;
					colorIds[color] = id++;
					Points[w, h] = colorIds[color];
				}
			}
		}
		Debug.Log(colors.Count);
		foreach (var color in colors.Keys)
		{
			Debug.Log($"color:{color},count:{colors[color]}");
		}
		for (int w = 0; w <= Width; w++)
		{
			for (int h = 0; h <= Height; h++)
			{
				var cids = GetColorIds(w, h).ToList();
				MPoint point = new(w, h, cids);
				Vector2Int pos = new(w, h);
				MiddlePoints[pos] = point;
				if (point.Type == PointType.Edge || point.Type == PointType.Point)
				{
					MPoint p1;
					if (pos.Up().TryGetPointFrom(MiddlePoints, out p1))
					{
						if (p1.Type == PointType.Edge || p1.Type == PointType.Point)
						{
							p1.neighborPoints.Add(point.Pos);
							point.neighborPoints.Add(p1.Pos);
						}
					}
					if (pos.Down().TryGetPointFrom(MiddlePoints, out p1))
					{
						if (p1.Type == PointType.Edge || p1.Type == PointType.Point)
						{
							p1.neighborPoints.Add(point.Pos);
							point.neighborPoints.Add(p1.Pos);
						}
					}
					if (pos.Left().TryGetPointFrom(MiddlePoints, out p1))
					{
						if (p1.Type == PointType.Edge || p1.Type == PointType.Point)
						{
							p1.neighborPoints.Add(point.Pos);
							point.neighborPoints.Add(p1.Pos);
						}
					}
					if (pos.Right().TryGetPointFrom(MiddlePoints, out p1))
					{
						if (p1.Type == PointType.Edge || p1.Type == PointType.Point)
						{
							p1.neighborPoints.Add(point.Pos);
							point.neighborPoints.Add(p1.Pos);
						}
					}
				}
			}
		}
		Debug.Log(22222);
		Debug.Log(MiddlePoints[new Vector2Int(158, 246)]);
		var EdgePoints = MiddlePoints.Values.AsParallel().
			Where(p => p.Type == PointType.Point || p.Type == PointType.Edge).
			SelectMany(p => p.Ids.Select(id => new { Id = id, Point = p })).
			GroupBy(p => p.Id, p => p.Point);
		List<List<MPoint>> EdgeAll = new();
		foreach (var results in EdgePoints)
		{
			int key = results.Key;
			Debug.Log(key);
			var cityPoints = results.AsParallel().SelectMany(
				p => p.Ids.Where(id => id != key).
				Select(id => new { Id = id, Point = p })).
				GroupBy(p => p.Id, p => p.Point);
			foreach (var edge in cityPoints)
			{
				int key2 = edge.Key;

				Debug.Log(key2);
				var points = edge.ToHashSet();
				var edgePoints = edge.ToDictionary(p => (p.Pos, p));
				List<List<MPoint>> Edges = new();
				Edges.Add(new List<MPoint>());
				var edgePs = points.Where(p => p.Type == PointType.Point).ToList();
				if (edgePs.Count == 2)
				{
					var startPoint = edgePs[0];

					var lastPoint = startPoint;
					//points.Remove(startPoint);
					bool stop = false;
					while (!stop)
					{
						var newPoint = startPoint.neighborPoints.Where(id => id != lastPoint.Pos).Select(p => MiddlePoints[p]).First(p => points.Contains(p));
						Edges[0].Add(newPoint);
						//points.Remove(newPoint);
						lastPoint = startPoint;
						startPoint = newPoint;
						if (newPoint.Type == PointType.Point)
						{
							stop = true;
						}
					}
					int i = 0;
					//while (points.Count > 0)
					//{
					//	var point = points.First();
					//	var firstPos = point.Pos;
					//	points.Remove(point);
					//	Edges.Add(new List<MPoint>());
					//	i++;
					//	while (!point.neighborPoints.Contains(firstPos) && points.Count > 0)
					//	{
					//		var newPoint = point.neighborPoints.Select(p => MiddlePoints[p]).First(p => points.Contains(p));
					//		Edges[i].Add(newPoint);
					//		points.Remove(newPoint);
					//		point = newPoint;
					//	}
					//}
					EdgeAll.AddRange(Edges);
				}
			}
		}
		EdgeAll.ForEach(e => CreateEdge(e.Select(p => p.Pos).ToList()));
	}

	private void CreateEdge(List<Vector2Int> vectors)
	{
		var ps = vectors.Select(v => new Vector3(v.x, v.y, 0)).ToArray();
		var edge = new GameObject().AddComponent<LineRenderer>();
		edge.sharedMaterial = Resources.Load<Material>("Materials/edge");
		edge.startWidth = 0.1f;
		edge.endWidth = 0.1f;
		edge.positionCount = ps.Length;
		edge.SetPositions(ps);

	}

	// Update is called once per frame
	void Update()
	{

	}
	private HashSet<int> GetColorIds(int w, int h)
	{
		HashSet<int> colors = new();
		colors.Add(GetColorIdByWH(w - 1, h - 1));
		colors.Add(GetColorIdByWH(w, h - 1));
		colors.Add(GetColorIdByWH(w - 1, h));
		colors.Add(GetColorIdByWH(w, h));
		return colors;
	}

	private int GetColorIdByWH(int w, int h)
	{
		if (w < 0 || h < 0 || w >= Width || h >= Height)
		{
			return -1;
		}
		return Points[w, h];
	}
}
public enum PointType
{
	Inner,//内部点
	Edge,//边界
	Point,//交汇点
}
public class MPoint
{
	public Vector2Int Pos;
	public List<int> Ids = new();
	public PointType Type;
	public HashSet<Vector2Int> neighborPoints = new();

	public MPoint(int x, int y, List<int> ids)
	{
		Pos = new(x, y);
		this.Ids = ids;
		switch (ids.Count)
		{
			case 1:
				Type = PointType.Inner;
				break;
			case 2:
				Type = PointType.Edge;
				break;
			case 3:
				Type = PointType.Point;
				break;
			case 4:
				Type = PointType.Point;
				break;
		}
	}
}
public static class TextureTool
{
	public static Vector2Int Up(this Vector2Int vector)
	{
		return vector + new Vector2Int(0, -1);
	}
	public static Vector2Int Down(this Vector2Int vector)
	{
		return vector + new Vector2Int(0, 1);
	}
	public static Vector2Int Left(this Vector2Int vector)
	{
		return vector + new Vector2Int(-1, 0);
	}
	public static Vector2Int Right(this Vector2Int vector)
	{
		return vector + new Vector2Int(1, 0);
	}

	public static bool TryGetPointFrom(this Vector2Int pos, Dictionary<Vector2Int, MPoint> dic, out MPoint p)
	{
		return dic.TryGetValue(pos, out p);
	}
	//public static int CompareTo(this  Vector3Int other)
	//{
	//    var dx = X - other.X;
	//    var dy = Y - other.Y;
	//    var dz = Z - other.Z;
	//    return dx * dx + dy * dy + dz * dz;
	//}
}