﻿using System.Collections.Generic;

public static class Tools
{
	public static void Add<K, T>(this Dictionary<K, List<T>> dic, K id, T t)
	{
		if (!dic.ContainsKey(id))
		{
			dic.Add(id, new List<T>());
		}
		dic[id].Add(t);
	}
}